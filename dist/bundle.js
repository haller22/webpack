/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./lib/dataLib.js":
/*!************************!*\
  !*** ./lib/dataLib.js ***!
  \************************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("var data = {}\n\nconsuntData = function (  ) {\n\n  return data\n}\n\nsetData = function ( sething ) {\n\n  data.set = sething\n}\n\nlistData = function (  ) {\n\n  return Object.keys ( data.set )\n}\n\ncreateFlag = function ( flagObject ) {\n\n  data[flagObject] = {}\n}\n\nsetByFlag = function ( sething, flag ) {\n\n  data[flag] = sething\n}\n\ngetByFlag = function ( flag ) {\n\n  return data[flag]\n}\n\nmodule.exports = {\n\n  consuntData: consuntData,\n  setData: setData,\n  listData: listData,\n  createFlag: createFlag,\n  setByFlag: setByFlag,\n  getByFlag: getByFlag\n}\n\n\n//# sourceURL=webpack:///./lib/dataLib.js?");

/***/ }),

/***/ "./src/index.js":
/*!**********************!*\
  !*** ./src/index.js ***!
  \**********************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("myFile = __webpack_require__ ( /*! ./myFile.js */ \"./src/myFile.js\" )\ndata = __webpack_require__ ( /*! ../lib/dataLib.js */ \"./lib/dataLib.js\" )\n\n\nconsole.log ( 'mensagem do index' );\n\n\nmyFile.abc (  );\n\ndata.setData ( { 'asdf' : 'conf' } )\n\nconsole.log ( data.listData (  ) )\n\nconsole.log ( data.consuntData (  ) )\n\ndata.createFlag ( 'hi' )\n\ndata.setByFlag ( {asdfasdf : 23}, 'hi' )\n\nconsole.log ( data.getByFlag ( 'hi' ) )\n\n\n//# sourceURL=webpack:///./src/index.js?");

/***/ }),

/***/ "./src/myFile.js":
/*!***********************!*\
  !*** ./src/myFile.js ***!
  \***********************/
/*! no static exports found */
/***/ (function(module, exports) {

eval("function abc (  ) {\n\n  console.log('abc');\n}\n\nmodule.exports = {\n\n  abc : abc\n}\n\n\n//# sourceURL=webpack:///./src/myFile.js?");

/***/ })

/******/ });